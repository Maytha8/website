#!/usr/bin/env python3
# Pelican configuration

# Basic details
AUTHOR = "The Debian Project"
SITENAME = "Debian"
# Only required in publishconf.py
SITEURL = ""

# Configuration
TIMEZONE = "Australia/Perth"
DELETE_OUTPUT_DIRECTORY = True
THEME = "themes/debian"

# Plugins
PLUGINS = ["postcss", "i18n_subsites"]
PLUGIN_PATHS = ["plugins"]
JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}

# Translation settings
DEFAULT_LANG = "en"
DIRECTION = "ltr"
I18N_GETTEXT_LOCALEDIR = "po/"
I18N_GETTEXT_DOMAIN = "debian_website"
I18N_SUBSITES = {
    "ar": {
        "SITENAME": "دبيان",
        "DIRECTION": "rtl",
    }
}
LANGUAGES = {
    "en": "English",
    "ar": "العربية",
}

# Feed settings
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/feed.rss'
FEED_ALL_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TAG_FEED_ATOM = None
TAG_FEED_RSS = None
TRANSLATION_FEED = None
TRANSLATION_FEED_ATOM = '{lang}/feeds/atom.xml'
TRANSLATION_FEED_RSS = '{lang}/feeds/feed.rss'

# Content settings
PATH = "content"
DEFAULT_PAGINATION = False

# URL settings
# RELATIVE_URLS = True
PAGE_URL = "{slug}/"
PAGE_SAVE_AS = "{slug}/index.html"
