import os
import subprocess
from fnmatch import fnmatch
from pelican import Pelican, signals


def postprocess_css(pelican: Pelican):
    PROJECT_PATH = os.path.abspath(os.path.join(pelican.path, ".."))
    POSTCSS_BIN = (
        os.path.join(PROJECT_PATH, "node_modules", ".bin", "postcss")
        if os.path.isfile(os.path.join(PROJECT_PATH, "node_modules", ".bin", "postcss"))
        else "postcss"
    )
    for path, _, files in os.walk(pelican.output_path):
        for name in files:
            path_file = os.path.join(path, name)
            if fnmatch(name, "*.css"):
                args = f"{POSTCSS_BIN} {path_file} --replace --config {PROJECT_PATH}"
                print(f"Running postcss: {args}")
                subprocess.run(
                    args,
                    shell=True,
                )


def register():
    signals.finalized.connect(postprocess_css)
