const path = require("node:path");

const plugins = [
  require("postcss-nested"),
  require("postcss-rtlcss"),
  require("autoprefixer"),
]

if (process.env["PELICAN_PUBLISH"] === "1") {
  plugins.push(require("postcss-url")({
    url: (asset) => {
      if (!asset.url.startsWith("/")) return asset.url;
      return path.join("/website", asset.pathname);
    },
  }))
}

module.exports = {
  plugins,
};
