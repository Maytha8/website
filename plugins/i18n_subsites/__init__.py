# SPDX-FileCopyrightText: 2014 Ondrej Grover
# SPDX-License-Identifier: AGPL-3.0-or-later
# Sourced from https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites
from .i18n_subsites import *
